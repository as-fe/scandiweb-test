<?php
include("../config.php");
include('../classes/Product.php');

if (isset($_GET['sku'])) {
    $product = new Product($con);
    $sku = $_GET['sku'];
    echo $product->checkSku($sku);
}
"\n";