<?php
require_once('Product.php');

class Book extends Product
{
    private $weight;

    public function __construct($con)
    {
        parent::__construct($con);
    }

    //Returns the markup for the book form as a json string
    public function bookInput(): string
    {
        $response = array();
        $response['field'] = '<div class="form-row">
        <label for="weight">Weight (KG)</label>
        <input type="text" name="weight" id="weight" class="number"  placeholder="Please, provide weight" required>
        </div>    
        <div id="weightError"></div>
        <p class="center"><strong>Please, provide weight</strong></p>';
        return json_encode($response);
    }
}
"\n";