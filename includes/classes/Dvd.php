<?php
require_once('Product.php');


class Dvd extends Product
{
    private $size;

    public function __construct($con)
    {
        parent::__construct($con);
    }

    //Returns the markup for the dvd form as a json string
    public function dvdInput(): string
    {
        $response = array();
        $response['field'] = '<div class="form-row">
        <label for="size">Size (MB)</label>
        <input type="text" name="size" id="size"  class="number" placeholder="Please, provide size" required>
        </div>   
        <div id="sizeError"></div>
        <p class="center"><strong>Please, provide size</strong></p>';
        return json_encode($response);
    }
}
"\n";