<?php
require_once('Product.php');


class Furniture extends Product
{
    private $height;
    private $width;
    private $length;

    public function __construct($con)
    {
        parent::__construct($con);
    }

    //Returns the markup for the furniture form as a json string
    public function furnitureInput(): string
    {
        $response = array();
        $response['field'] = '<div class="form-row">
        <label for="height">Height (CM)</label>
        <input type="text" name="height" id="height"  class="number"  placeholder="Please, provide height" required>
        </div>
        <div id="heightError"></div>
        <div class="form-row">
        <label for="width">Width (CM)</label>
        <input type="text" name="width" id="width"  class="number"  placeholder="Please, provide width" required>
        </div>
        <div id="widthError"></div>
        <div class="form-row">
        <label for="length">Length (CM)</label>
        <input type="text" name="length" id="length"  class="number"  placeholder="Please, provide length" required>
        </div>
        <div id="lengthError"></div>
        <p class="center"><strong>Please, provide dimensions</strong></p>';
        return json_encode($response);
    }
}
"\n";