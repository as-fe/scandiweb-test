<?php
require_once('Db.php');


class Product extends Db
{
    private $con;
    private $id;
    private $sku;
    private $name;
    private $price;
    private $size;
    private $weight;
    private $height;
    private $width;
    private $length;
    private $productids;
    private $tablename;
    private $fields;


    public function __construct($con)
    {
        $tablename = $this->getTable();
        $fields = $this->getFields();
        parent::__construct($con, $tablename, $fields);

    }

    //Sets the tablename for the given class
    public function getTable(): string
    {
        $this->tablename = 'products';
        return $this->tablename;
    }

    //Sets the fields in the given dB table
    public function getFields(): array
    {
        $this->fields = array('sku', 'name', 'price', 'type', 'size', 'weight', 'height', 'width', 'length');
        return $this->fields;
    }
    
     //Returns an array of all the product ids
    public function getProductIds(): array
    {
        $this->productlist = $this->getData(' ', '');
        $this->productids = array();
        foreach ($this->productlist as $id) {
            array_push($this->productids, $id['product_id']);
        }
        return $this->productids;
    }

     //Returns the current id of the product
    public function getId(): int
    {
        $this->id = $this->getData(' ', '')[0]['product_id'];
        return $this->id;
    }

     //Returns the sku of the product based on the id
     //@param $id - id of the product 
    public function getSku($id): string
    {
        $this->sku = $this->getData("WHERE product_id=?", $id)[0]['product_sku'];
        return $this->sku;
    }

    //Returns the name of the product based on id
    //@param $id - id of the product
    public function getName($id): string
    {
        $this->name = $this->getData("WHERE product_id=?", $id)[0]['product_name'];
        return $this->name;
    }

     //Returns the price of the product based on id
    //@param $id - id of the product
    public function getPrice($id): string
    {
        $price = $this->getData("WHERE product_id=?", $id)[0]['product_price'];
        $this->price = number_format($price, 2, '.', '');
        return $this->price;
    }

    //Checks if the product with the given id has a value for size and if so returns it
    public function getSize($id): float
    {
        $this->size = $this->getData("WHERE product_id=?", $id)[0]['product_size'];
        return $this->size;
    }
    
    //Checks if the product with the given id has a value for weight and if so returns it
    public function getWeight($id): float
    {
        $this->weight = $this->getData("WHERE product_id=?", $id)[0]['product_weight'];
        return $this->weight;
    }

    //Checks if the product with the given id has a value for height and if so returns it
    public function getHeight($id): float
    {
        $this->height = $this->getData("WHERE product_id=?", $id)[0]['product_height'];
        return $this->height;
    }
 
    //Checks if the product with the given id has a value for width and if so returns it
    public function getWidth($id): float
    {
        $this->width = $this->getData("WHERE product_id=?", $id)[0]['product_width'];
        return $this->width;
    }
 
     //Checks if the product with the given id has a value for length and if so returns it
    public function getLength($id): float
    {
        $this->length = $this->getData("WHERE product_id=?", $id)[0]['product_length'];
        return $this->length;
    }
  
    //Checks if the given sku exists in the dB, in order to prevent duplicate insertion or failure upon insert
    //@param $sku - the value inputed in the sku field
    public function checkSku($sku): string
    {
        $this->skuCheck = $this->getData("WHERE product_sku=?", $sku);
        $response = array();
        if ($this->skuCheck != null) {
            $response['field'] = '<p class="error">Please insert a different SKU, this one is already in use</p>';
        } else {
            $response['field'] = '';
        }
        return json_encode($response);
    }

    //If there is a value for size, returns the markup for outputting the size in the product card
    public function checkDvd($id): string
    {
        return $this->getSize($id) != 0 ? 'Size: ' . $this->getSize($id) . ' MB' : '';
    }
    
    //If there is a value for weight, returns the markup for outputting the weight in the product card
    public function checkBook($id): string
    {
        return $this->getWeight($id) != 0 ? 'Weight: ' . $this->getWeight($id) . ' KG' : '';
    }

    //If there is a value for height, returns the markup for outputting the dimensions in the product card
    public function checkFurniture($id): string
    {
        return $this->getHeight($id) != 0 ? 'Dimension: ' . $this->getHeight($id) . 
        ' x' . $this->getWidth($id) . ' x' . $this->getLength($id) : '';
    }

    //Calls the parent method to insert the product in the database
    //@param $fieldanmes - the $_POST array
    public function insertProduct($fieldnames)
    {
        $this->insertData($fieldnames);
    }

    //Calls the parent method to delete the products in the database
    //@param $fieldanmes - the $_POST['fieldList'] array
    public function deleteProduct($fieldarray)
    {
        $this->deleteData($fieldarray);
    }

    public function retrieveAllProducts(): string
    {
        $productids=$this->getProductIds();
        $response=array();
        $names=array();
        $prices=array();
        $skus=array();
        $sizes=array();
        $weights=array();
        $dimensions=array();
        foreach ($productids as $id) {
            array_push($names, htmlentities($this->getName($id)));
            array_push($skus, htmlentities($this->getSku($id)));
            array_push($prices, htmlentities($this->getPrice($id)));
            array_push($sizes, htmlentities($this->checkDvd($id)));
            array_push($weights, htmlentities($this->checkBook($id)));
            array_push($dimensions, htmlentities($this->checkFurniture($id)));
        }
        array_push($response, $productids);
        array_push($response, $names);
        array_push($response, $skus);
        array_push($response, $prices);
        array_push($response, $sizes);
        array_push($response, $weights);
        array_push($response, $dimensions);
        return json_encode($response);
    }
}
"\n";
