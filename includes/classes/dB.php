<?php

class Db
{
    private $con;
    private $tablename;
    private $fields;
    private $data_array;
    private $concat;


    public function __construct($con, $tablename, $fields)
    {
        $this->con = $con;
        $this->tablename = $tablename;
        $this->fields = $fields;
         //@var $concat - Assumes the naming convention for the dB is respected:
        //Tablename: things
        //Column names: thing_x
        //Takes the name of the table, strips the last character and concatenates the underscore
        $this->concat=rtrim($this->tablename, 's') . '_';

    }

    //Retrieves an array of the data found in dB
    //@param $where - the condition used to search in the dB
    public function getData($where, $val): array
    {
        $this->data_array = array();
        $this->sql = "SELECT * FROM $this->tablename $where";
        $value=$val;
        
        $stmt = $this->con->prepare($this->sql);
        if($where != ' ') {
            $stmt->bind_param('s', $value);
        } else {
            $value=null;
        }
        
        $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {
            $this->data_array[] = $row;
        }
        return $this->data_array;
    }

    //Inserts data in the dB 
    //@param $fieldnames - an array of the names of the fields to be inserted
    public function insertData($fieldnames)
    {
        $fieldlist = $this->fields;
        foreach ($fieldnames as $field => $fieldvalue) {
            //Clause to escape the 'submit' field
            if (!in_array($field, $fieldlist)) {
                unset($fieldnames[$field]);
            }
        }
        $this->sql = "INSERT INTO $this->tablename SET ";
               
        foreach ($fieldnames as $item => $value) {
            $this->sql .= "$this->concat" . "$item=?, ";
        }
        //Removes the last ',' in the dynamically built sql statement
        $this->sql = rtrim($this->sql, ', ');
        $stmt=$this->con->prepare($this->sql);
        
        $values=[];
        $types=[];
        
        foreach ($fieldnames as $item => $value) {
            $cleanValue = $this->cleanData($value);
            $values[] = $cleanValue;
            $type=substr(gettype($cleanValue), 0, 1);
            array_push($types, $type);
            $parameter=implode('', $types);
        }

        $stmt->bind_param($parameter, ...$values);
      
        $stmt->execute();
    }

    //Cleans the data to be inserted in the dB
    //@param $data - the field value in the submitted form
    public function cleanData($data): string
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    //Deletes data from dB
    //@param $fieldarray - an array of the ids of the products to be deleted
    public function deleteData($fieldarray)
    {
        foreach ($fieldarray as $id => $value) {
            $this->sql = "DELETE FROM $this->tablename WHERE $this->concat" . "id=?";
            $stmt=$this->con->prepare($this->sql);
            $stmt->bind_param('d', $value);
            $stmt->execute();
        }
    }

}
"\n";