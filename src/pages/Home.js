import ReactDOM from "react-dom";
import Header from "../components/Header";
import Footer from "../components/Footer";
import ProductCard from "../components/ProductCard";
import React from "react";
import { useEffect } from "react";

function Home() {
  function retrieve() {
    let xmlhttp;
    document.getElementById("products").innerHTML = "";

    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new window.ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        const obj = JSON.parse(this.responseText);

        for (let i = 0; i < obj[0].length; i++) {
          const id = Math.random();
          const d = document.createElement("div");
          d.id = id;
          d.classList.add("products-card");
          document.getElementById("products").appendChild(d);
          ReactDOM.render(
            <ProductCard
              id={obj[0][i]}
              name={obj[1][i]}
              price={obj[3][i]}
              sku={obj[2][i]}
              size={obj[4][i]}
              weight={obj[5][i]}
              dimension={obj[6][i]}
            />,
            document.getElementById(id)
          );
        }
      }
    };
    xmlhttp.open("GET", "/includes/handlers/retrieveHandler.php", true);
    xmlhttp.send();
  }

  function deleteProduct(event) {
    event.preventDefault();
    const checkboxes = document.querySelectorAll(".delete-checkbox");
    const form = document.getElementById("delete_form");

    checkboxes.forEach(function check(cb) {
      if (cb.checked == true) {
        let xmlhttp;

        if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
        } else {
          xmlhttp = new window.ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
          if (this.readyState == 4 && this.status == 200) {
            window.location.reload();
          }
        };
        xmlhttp.open("POST", "/includes/handlers/deleteHandler.php", true);
        xmlhttp.send(new FormData(form));
      }
    });
  }

  useEffect(retrieve);

  return (
    <section>
      <form method="post" className="form-delete" id="delete_form">
        <Header
          title="Product List"
          url="/add-product"
          linktitle="Add a product page"
          linktext="ADD"
          linkclass="btn btn-add"
          buttonformname="delete"
          buttonclass="btn btn-delete"
          buttonid="delete-product-btn"
          buttonname="MASS DELETE"
          action={deleteProduct}
        />
        <div className="products" id="products"></div>
      </form>
      <Footer />
    </section>
  );
}

export default Home;
