import AddProductForm from "../components/AddProductForm";
import Footer from "../components/Footer";

function AddProduct() {
  return (
    <main>
      {" "}
      <AddProductForm />
      <Footer />
    </main>
  );
}

export default AddProduct;
