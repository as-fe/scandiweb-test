import { Route, Switch } from "react-router-dom";
import "./sass/main.css";
import AddProduct from "./pages/AddProduct";
import Home from "./pages/Home";

function App() {
  return (
    <Switch>
      <Route path="/" exact>
        <Home />
      </Route>
      <Route path="/add-product" exact>
        <AddProduct />
      </Route>
    </Switch>
  );
}

export default App;
