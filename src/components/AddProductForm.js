import Header from "../components/Header";

function AddProductForm() {
  const numberRegex = /^[0-9.]+$/;
  const textRegex = /^[a-zA-z0-9 ]+$/;

  //Function to retrieve the different forms for product input
  //@param concat - a string that indicates type and is used to identify the correct handler file
  function typeSwitcher(event) {
    const concat = event.target.value;
    let xmlhttp;

    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new window.ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        const obj = JSON.parse(this.responseText);
        document.getElementById("dynamicFields").innerHTML = obj.field;
        const inputs = document
          .getElementById("dynamicFields")
          .getElementsByTagName("input");
        [...inputs].forEach((inp) =>
          inp.addEventListener("blur", validateField)
        );
        [...inputs].forEach((inp) =>
          inp.addEventListener("keyup", validateField)
        );
      }
    };
    xmlhttp.open(
      "GET",
      "/includes/handlers/" + concat + "Handler.php?type=" + concat,
      true
    );
    xmlhttp.send();
  }

  //Input validation
  //Disables the save button
  function disableSaveButton() {
    document
      .getElementById("add-product-btn")
      .setAttribute("disabled", "disabled");
    document.getElementById("add-product-btn").classList.add("disabled");
  }

  //Enables the form button if there are no current errors in the form
  function enableSaveButton() {
    if (document.querySelectorAll(".error").length === 0) {
      document.getElementById("add-product-btn").removeAttribute("disabled");
      document.getElementById("add-product-btn").classList.remove("disabled");
    }
  }

  //Validates the input in the given field
  //@param event - the event used to prevent validation from firing up when form is navigated with tab
  function validateField(event) {
    const field = event.target;
    if (event.keyCode === 9) {
      return;
    } else {
      if (field.value == "") {
        field.classList.add("error");
        document.getElementById(field.name + "Error").innerHTML =
          "<p class='error' id='" +
          field.name +
          "err'>Please, submit required data</p>";
        disableSaveButton();
      } else {
        field.classList.remove("error");
        document.getElementById(field.name + "Error").innerHTML = "";
        enableSaveButton();

        if (
          (field.classList.contains("number") &&
            !numberRegex.test(field.value)) ||
          (field.classList.contains("text") && !textRegex.test(field.value))
        ) {
          field.classList.add("error");
          document.getElementById(field.name + "Error").innerHTML =
            "<p class='error' id='" +
            field.name +
            "err'>Please, provide the data of indicated type</p>";
          disableSaveButton();
        } else {
          field.classList.remove("error");
          document.getElementById(field.name + "Error").innerHTML = "";
          enableSaveButton();
        }
      }
    }
  }

  //Checks if the value inserted in the SKU field is already in the database
  //@param event - the event that is passed to the validateField function
  function skuChecker(event) {
    let xmlhttp;
    const sku = event.target.value;

    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new window.ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        const obj = JSON.parse(this.responseText);
        document.getElementById("skuError").innerHTML = obj.field;
        if (document.getElementById("skuError").innerHTML !== "") {
          document.getElementById("skuError").classList.add("error");
          document.getElementById("sku").classList.add("error");
          disableSaveButton();
        } else {
          validateField(event);
          document.getElementById("skuError").classList.remove("error");
        }
      }
    };
    xmlhttp.open("GET", "/includes/handlers/skuHandler.php?sku=" + sku, true);
    xmlhttp.send();
  }

  function addProduct(event) {
    event.preventDefault();
    const form = document.getElementById("product_form");
    let xmlhttp;

    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new window.ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        const obj = JSON.parse(this.responseText);
        window.location.replace("/");
      }
    };
    xmlhttp.open("POST", "/includes/handlers/addProductHandler.php", true);
    xmlhttp.send(new FormData(form));
  }

  return (
    <form method="post" id="product_form" className="form-add">
      <Header
        title="Product Add"
        url="/"
        linktitle="Back to the list"
        linktext="CANCEL"
        linkclass="btn btn-delete"
        buttonformname="add"
        buttonclass="btn btn-add"
        buttonid="add-product-btn"
        buttonname="Save"
        action={addProduct}
      />
      <section className="form">
        <div className="form-row">
          <label htmlFor="sku">SKU</label>
          <input
            type="text"
            name="sku"
            id="sku"
            className="text"
            onKeyUp={skuChecker}
            onBlur={skuChecker}
            required
          />
        </div>
        <div id="skuError"></div>
        <div className="form-row">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            name="name"
            id="name"
            className="text"
            onKeyUp={validateField}
            onBlur={validateField}
            required
          />
        </div>
        <div id="nameError"></div>
        <div className="form-row">
          <label htmlFor="price">Price ($)</label>
          <input
            type="text"
            name="price"
            id="price"
            className="number"
            onKeyUp={validateField}
            onBlur={validateField}
            required
          />
        </div>
        <div id="priceError"></div>
        <div className="form-row">
          <label htmlFor="productType">Type Switcher</label>
          <select
            name="type"
            id="productType"
            onChange={typeSwitcher}
            onBlur={validateField}
            defaultValue={"default"}
            required
          >
            <option disabled value="default">
              Select one option
            </option>
            <option id="DVD" value="dvd">
              DVD
            </option>
            <option id="Furniture" value="furniture">
              Furniture
            </option>
            <option id="Book" value="book">
              Book
            </option>
          </select>
        </div>
        <div id="typeError"></div>
        <div id="dynamicFields"></div>
      </section>
      <hr />
    </form>
  );
}

export default AddProductForm;
