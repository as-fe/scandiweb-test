import { Link } from "react-router-dom";

function Header(props) {
  return (
    <div>
      <section className="header">
        <h1 className="header-title">{props.title}</h1>
        <div className="btn-container">
          <Link
            to={props.url}
            className={props.linkclass}
            title={props.linktitle}
          >
            {props.linktext}
          </Link>
          <button
            type="submit"
            name={props.buttonformname}
            className={props.buttonclass}
            id={props.buttonid}
            onClick={props.action}
          >
            {props.buttonname}
          </button>
        </div>
      </section>
      <hr />
    </div>
  );
}

export default Header;
