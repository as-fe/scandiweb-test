function ProductCard(props) {
  return (
    <div>
      <input
        type="checkbox"
        name="productList[]"
        className="delete-checkbox"
        value={props.id}
      />
      <div className="products-card_container">
        <p className="products-sku">{props.sku}</p>
        <h2 className="products-name">{props.name}</h2>
        <p className="products-price">{props.price} $</p>
        <p className="products-details">{props.size}</p>
        <p className="products-details">{props.weight}</p>
        <p className="products-details">{props.dimension}</p>
      </div>
    </div>
  );
}

export default ProductCard;
