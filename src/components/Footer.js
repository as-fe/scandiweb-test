function Footer() {
  return (
    <section className="footer">
      <hr />
      <p className="footer-text">Scandiweb Test assignment</p>
    </section>
  );
}

export default Footer;
